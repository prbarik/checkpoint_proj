terraform {
  backend "http" {
  }
  required_providers {
      gitlab = {
          source = "gitlabhq/gitlab"
          version = "~> 3.1"
      }
  }
}

variable "gitlab_access_token" {
    type = string
}

provider "gitlab" {
    token = var.gitlab_access_token
}

data "gitlab_project" "Checkpoint_Proj" {
    id = 28873679
}

# Add a Variable to the project
resource "gitlab_project_variable" "Checkpoint_Proj_Variable" {
    project = data.gitlab_project.Checkpoint_Proj.id
    key = "example_variable"
    value = "Hello From Terraform!!"
}